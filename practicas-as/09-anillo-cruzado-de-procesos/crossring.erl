-module(crossring).

%% PUBLIC API
-export([start/3]).

-define(CROSSRING, crossring).


% crear `ProcNum` procesos concectados un forma de anillo cruzado
start(ProcNum, MsgNum, Message) when (ProcNum > 0), (MsgNum > 0) ->
    true = register(?CROSSRING, spawn(fun() -> init_maestro(ProcNum-1, MsgNum, Message) end)),
    ok.


init_maestro(0, MsgNum, Message) ->
    whereis(?CROSSRING) ! {MsgNum-1, Message, any},
    loop(whereis(?CROSSRING));

init_maestro(ProcNum, MsgNum, Message) ->
    Half = ProcNum div 2,
    Rest = ProcNum rem 2,
    case {Half > 0, Half+Rest > 0} of
        {true, true} ->
            Izquierda = spawn(fun() -> init(Half-1) end),
            Derecha = spawn(fun() -> init(Half+Rest-1) end),
           
            Izquierda ! {MsgNum-1, Message, Izquierda},
            loop_maestro(Izquierda, Derecha);
        {false, true} ->
            Derecha = spawn(fun() -> init(Half+Rest-1) end),
            Derecha ! {MsgNum-1, Message, any},
            loop(Derecha)
       
    end.


loop(Siguiente) ->
    receive
        {0, _Message, _From} ->
            
            Siguiente ! stop,
            ok;
        {MsgNum, Message, From} ->
           
            Siguiente ! {MsgNum-1, Message, From},
            loop(Siguiente);
        stop ->
            
            catch Siguiente ! stop,
            ok;
        _Other ->
            
            loop(Siguiente)
    end.


init(0) ->
    loop(whereis(?CROSSRING));
init(ProcNum) ->
    Siguiente = spawn(fun() -> init(ProcNum-1) end),
    loop(Siguiente).


loop_maestro(Izquierda, Derecha) ->
    receive
        {0, _Message, _From} ->
           
            Izquierda ! stop,
            Derecha ! stop,
            unregister(?CROSSRING),
            ok;
        {MsgNum, Message, right} ->
            
            Izquierda ! {MsgNum-1, Message, Izquierda},
            loop_maestro(Izquierda, Derecha);
        {MsgNum, Message, Izquierda} ->
            
            Derecha ! {MsgNum-1, Message, right},
            loop_maestro(Izquierda, Derecha);
        stop ->
            
            Izquierda ! stop,
            Derecha ! stop,
            unregister(?CROSSRING),
            ok;
        _Other ->
           
            loop_maestro(Izquierda, Derecha)
    end.
