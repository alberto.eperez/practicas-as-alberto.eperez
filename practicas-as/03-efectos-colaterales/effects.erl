-module(effects).
-export([print/1, even_print/1]).

print(N) -> print(N,1).

print(N, M) when M=<N -> io:format("~p ",[M]), print(N,M+1);
print(_, _) -> io:format("~n",[]).



even_print(N) -> even_print(N,1).

even_print(N,M) when M=<N -> print_e(N,M);
even_print(N,M) when M>N -> io:format("~n",[]).

print_e(N,M) when M rem 2 == 0 -> io:format("~p ",[M]), even_print(N,M+1);
print_e(N,M) when M rem 2 == 1 -> even_print(N, M+1). 
