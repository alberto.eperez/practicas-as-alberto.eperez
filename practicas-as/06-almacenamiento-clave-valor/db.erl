-module(db).

-export([new/0, write/3, delete/2, read/2, match/2, destroy/1]).


%% reverse/1 del módulo manipulating(ej. 04)
reverse(L) -> reverse(L,[]).

reverse([],Acc) -> Acc;
reverse([H|T],Acc) -> reverse(T, [H|Acc]).


%%% API %%%%

%% `db:new() -> DbRef`
%% Crea el almacen de datos.
new() -> [].


%% `db:write(Key, Element, DbRef) -> NewDbRef`
%% Inserta un nuevo elemento en el almacen `DbRef`.
write(Key, Element, DbRef) -> 
	[{Key,Element}|DbRef].


%% `db:delete(Key, DbRef) -> NewDbRef`
%% Elimina la primera ocurrencia de la clave `Key` en el almacen `DbRef`.
delete(Key, DbRef) -> 
	delete(Key, DbRef, []).

delete(_Key, [], Acc) -> reverse(Acc);
delete(Key, [{Key, _Element}|T], Acc) -> delete(Key, T, Acc);
delete(Key, [H|T], Acc) -> delete(Key, T, [H|Acc]).


%% `db:read(Key, DbRef) -> {ok, Element} | {error, instance}`
%% Recupera la primera ocurrencia de la clave `Key` en el almacen `DbRef`,
%% o devuelve un valor de error sin no existe. 
read(_Key, []) -> {error, instance};
read(Key, [{Key, Element}|_T]) -> {ok, Element};
read(Key, [{_Key, _Element}|T]) -> read(Key, T).


%% `db:match(Element, DbRef) -> [Key, ..., KeyN]`
%% Recupera todas las claves que contengan el valor `Element`.
match(Element, DbRef) -> match(Element, DbRef, []).

match(_Element, [], Elements) -> reverse(Elements);
match(Element, [{Key, Element}|T], Elements) -> match(Element, T, [Key|Elements]);
match(Element, [_H|T], Elements) -> match(Element, T, Elements).


%% `db:destroy(DbRef) -> ok`
%% Elimina el almacenamiento `DbRef`.
destroy(_DbRef) -> ok.
