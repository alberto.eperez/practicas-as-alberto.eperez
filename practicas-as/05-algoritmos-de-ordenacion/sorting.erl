-module(sorting).
-export([quicksort/1, mergesort/1]).

part([], _, L1, L2) -> {L1,L2};
part([H|T], Piv, L1, L2) ->
	if H<Piv -> part(T, Piv, [H|L1], L2);
		true -> part(T, Piv, L1, [H|L2])
	end.


quicksort([]) -> [];
quicksort([H|T]) ->
	{L1,L2} = part(T, H, [], []),
	quicksort(L1)++[H]++quicksort(L2).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

merge(A, []) -> A;
merge([], B) -> B;
merge([Ha|Ta], [Hb|Tb]) ->
  if
    Ha < Hb -> [Ha | merge(Ta, [Hb|Tb])];
    true -> [Hb | merge([Ha|Ta], Tb)]
  end.

mergesort([]) -> [];
mergesort([N]) -> [N];
mergesort(L) ->
  {A, B} = lists:split(trunc(length(L)/2), L),
  merge(mergesort(A), mergesort(B)).
